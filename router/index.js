const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');

router.get('/', (req,res)=>{

    const valores={
     des:req.query.des,
     edad:req.query.edad,
     precio:req.query.precio,
     NB:req.query.NB,
     tipo:req.query.tipo
    }
    
    res.render('index.html', valores);
})

router.post('/', (req,res)=>{
    const valores ={
        NB:req.body.NB,
        des:req.body.des,
        edad:req.body.edad,
        precio:req.body.precio,
        tipo:req.body.tipo,
        sub:req.body.sub,
        imp:req.body.imp,
        desc:req.body.desc,
        tot:req.body.tot
    }
    res.render('index.html', valores);
})

router.get('/preexa', (req,res)=>{

    const valores={
     des:req.query.des,
     edad:req.query.edad,
     precio:req.query.precio,
     NB:req.query.NB,
     tipo:req.query.tipo
    }
    
    res.render('preexa.html', valores);
})

router.post('/preexa', (req,res)=>{
    const valores ={
        NB:req.body.NB,
        des:req.body.des,
        edad:req.body.edad,
        precio:req.body.precio,
        tipo:req.body.tipo,
        sub:req.body.sub,
        imp:req.body.imp,
        desc:req.body.desc,
        tot:req.body.tot
    }
    res.render('preexa.html', valores);
})
router.use((req,res,next)=>{

        res.status(404).sendFile(__dirname + '/public/error.html')
    
    })
module.exports=router;

