const express = require('express');
const misRutas = require('./router/index');
const bodyparser = require('body-parser');
const path = require('path');


const app = express();

app.use(express.static(__dirname + "/public"));
app.use(bodyparser.urlencoded({extended:true}));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(misRutas);



    const puerto = 500;
app.listen(puerto,()=>{
    console.log("iniciando puerto 500")
})
